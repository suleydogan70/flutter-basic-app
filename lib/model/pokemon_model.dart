class Pokemon {
  String id;
  String name;
  String imageURL;
  List types;
  String hp;
  String rarity;
  List attacks;
  List resistances;
  List weaknesses;

  Pokemon(this.id, this.name, this.imageURL, this.types, this.hp, this.rarity,
      this.attacks, this.resistances, this.weaknesses);

  Pokemon.fromJSON(Map json) {
    id = json['id'] ?? '';
    name = json['name'] ?? '';
    imageURL = json['imageUrl'] ?? '';
    types = json['types'] ?? [];
    hp = json['hp'] ?? '';
    rarity = json['rarity'] ?? '';
    attacks = json['attacks'] ?? [];
    resistances = json['resistances'] ?? [];
    weaknesses = json['weaknesses'] ?? [];
  }

  @override
  String toString() {
    return 'Pokemon{id: $id, name: $name, imageURL: $imageURL, types: $types, hp: $hp, rarity: $rarity, attacks: $attacks, resistances: $resistances, weaknesses: $weaknesses}';
  }
}
