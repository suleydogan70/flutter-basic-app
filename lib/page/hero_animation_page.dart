import 'package:flutter/material.dart';
import 'package:hello_world_flutter/page/login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HeroAnimationPage extends StatefulWidget {
  @override
  _HeroAnimationPageState createState() => _HeroAnimationPageState();
}

class _HeroAnimationPageState extends State<HeroAnimationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            IconButton(
              onPressed: _logOut,
              icon: Icon(Icons.play_arrow),
            ),
            Container(
              width: 4.0,
            ),
            Text('Hero animation'),
          ],
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text(
            'VROOOOOOOOOOOOOOOOOOOOOOOOM',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 38.0,
            ),
          ),
          Hero(
            tag: 'image',
            child: Image.asset("assets/kebab.jpg", fit: BoxFit.cover),
          ),
        ],
      ),
    );
  }

  _logOut() async {
    print('log out');
    final pref = await SharedPreferences.getInstance();
    await pref.clear();
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LoginPage()));
  }
}
