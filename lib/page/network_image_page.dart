import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class NetWorkImagePage extends StatefulWidget {
  @override
  _NetWorkImagePageState createState() => _NetWorkImagePageState();
}

class _NetWorkImagePageState extends State<NetWorkImagePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Network Image'),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Tu veux voir ma grosse voiture ?"),
          ),
          CachedNetworkImage(
            imageUrl:
                "https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/2FA696E26739A45020434268539398688A4194DEB0C445119ADBB58C347A4AF8/scale?aspectRatio=1.78&format=jpeg",
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
        ],
      ),
    );
  }
}
