import 'package:flutter/material.dart';

class CalculatorPage extends StatefulWidget {
  @override
  _CalculatorPageState createState() => _CalculatorPageState();
}

class _CalculatorPageState extends State<CalculatorPage> {
  List<String> row1 = ['AC', '+/-', '%', '/'];
  List<String> row2 = ['7', '8', '9', 'X'];
  List<String> row3 = ['4', '5', '6', '-'];
  List<String> row4 = ['3', '2', '1', '+'];
  List<String> row5 = ['0', '.', '='];

  TextEditingController _result = TextEditingController();
  TextEditingController _allCalc = TextEditingController();
  double _initialValue;
  String _operator;
  bool _resultAlreadyCalled = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Icon(Icons.apps),
            Container(
              width: 12,
            ),
            Text('Calculator'),
          ],
        ),
        centerTitle: true,
      ),
      body: Container(
        color: Colors.black,
        child: Column(
          children: <Widget>[
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            TextField(
                              textAlign: TextAlign.end,
                              controller: _allCalc,
                              enabled: false,
                              style: TextStyle(
                                fontSize: 21.0,
                                color: Colors.white,
                              ),
                            ),
                            TextField(
                              textAlign: TextAlign.end,
                              controller: _result,
                              enabled: false,
                              style: TextStyle(
                                fontSize: 48.0,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 2,
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            _generateButtons(
                                row1[0], Color.fromRGBO(181, 181, 181, 1)),
                            _generateButtons(
                                row1[1], Color.fromRGBO(181, 181, 181, 1)),
                            _generateButtons(
                                row1[2], Color.fromRGBO(181, 181, 181, 1)),
                            _generateButtons(
                                row1[3], Color.fromRGBO(254, 156, 16, 1)),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            _generateButtons(
                                row2[0], Color.fromRGBO(65, 65, 65, 1)),
                            _generateButtons(
                                row2[1], Color.fromRGBO(65, 65, 65, 1)),
                            _generateButtons(
                                row2[2], Color.fromRGBO(65, 65, 65, 1)),
                            _generateButtons(
                                row2[3], Color.fromRGBO(254, 156, 16, 1)),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            _generateButtons(
                                row3[0], Color.fromRGBO(65, 65, 65, 1)),
                            _generateButtons(
                                row3[1], Color.fromRGBO(65, 65, 65, 1)),
                            _generateButtons(
                                row3[2], Color.fromRGBO(65, 65, 65, 1)),
                            _generateButtons(
                                row3[3], Color.fromRGBO(254, 156, 16, 1)),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            _generateButtons(
                                row4[0], Color.fromRGBO(65, 65, 65, 1)),
                            _generateButtons(
                                row4[1], Color.fromRGBO(65, 65, 65, 1)),
                            _generateButtons(
                                row4[2], Color.fromRGBO(65, 65, 65, 1)),
                            _generateButtons(
                                row4[3], Color.fromRGBO(254, 156, 16, 1)),
                          ],
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            _generateButtons(
                                row5[0], Color.fromRGBO(65, 65, 65, 1)),
                            _generateButtons('', Color.fromRGBO(65, 65, 65, 1)),
                            _generateButtons(
                                row5[1], Color.fromRGBO(65, 65, 65, 1)),
                            _generateButtons(
                                row5[2], Color.fromRGBO(254, 156, 16, 1)),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _generateButtons(buttonText, Color color) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: ClipOval(
          child: FlatButton(
            color: color,
            padding: EdgeInsets.all(20.0),
            child: Text(buttonText,
                style: TextStyle(fontSize: 31.0, color: Colors.white)),
            onPressed: () {
              switch (buttonText) {
                case '%':
                case '/':
                case 'X':
                case '-':
                case '+':
                  {
                    if (_result.text == '') {
                      print('break the rules');
                      break;
                    } else {
                      if (_initialValue == null) {
                        _initialValue = double.parse(_result.text);
                      } else {
                        if (!_resultAlreadyCalled) {
                          _getResult(operator: buttonText);
                        }
                      }
                      _result.text = '';
                      _operator = buttonText;
                    }
                    break;
                  }
                case '=':
                  {
                    _getResult(submit: true);
                    _resultAlreadyCalled = true;
                  }
                  break;
                case 'AC':
                  {
                    _result.text = '';
                    _initialValue = null;
                    _operator = null;
                    _allCalc.text = '';
                  }
                  break;
                case '+/-':
                  {
                    if (_result.text != '') {
                      if (_result.text[0] == '-') {
                        _result.text = _result.text.substring(1);
                        _allCalc.text = _allCalc.text.substring(1);
                      } else {
                        _result.text = '-' + _result.text;
                        _allCalc.text = '-' + _allCalc.text;
                      }
                    }
                  }
                  break;
                default:
                  {
                    if (_resultAlreadyCalled) {
                      _resultAlreadyCalled = false;
                      _result.text = buttonText;
                      _allCalc.text = buttonText;
                      _allCalc.text = '';
                    } else {
                      _result.text += buttonText;
                    }
                  }
              }
              if (buttonText != '+/-' &&
                  buttonText != 'AC' &&
                  buttonText != '=') {
                _allCalc.text += buttonText;
              }
            },
          ),
        ),
      ),
    );
  }

  _getResult({operator = null, submit = false}) {
    print('getresult');
    switch (_operator) {
      case '%':
        {
          _result.text =
              (_initialValue % double.parse(_result.text)).toString();
        }
        break;
      case '/':
        {
          _result.text =
              (_initialValue / double.parse(_result.text)).toString();
        }
        break;
      case 'X':
        {
          _result.text =
              (_initialValue * double.parse(_result.text)).toString();
        }
        break;
      case '-':
        {
          _result.text =
              (_initialValue - double.parse(_result.text)).toString();
        }
        break;
      case '+':
        {
          _result.text =
              (_initialValue + double.parse(_result.text)).toString();
        }
        break;
    }
    _allCalc.text = _result.text;
    _initialValue = double.parse(_result.text);
    _operator = operator ?? null;
  }
}
