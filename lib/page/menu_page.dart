import 'package:flutter/material.dart';
import 'package:hello_world_flutter/page/calculator_page.dart';
import 'package:hello_world_flutter/page/degree_converter_page.dart';
import 'package:hello_world_flutter/page/exercice_page.dart';
import 'package:hello_world_flutter/page/hero_animation_page.dart';
import 'package:hello_world_flutter/page/image_gallery_page.dart';
import 'package:hello_world_flutter/page/login_page.dart';
import 'package:hello_world_flutter/page/long_list_page.dart';
import 'package:hello_world_flutter/page/network_image_page.dart';
import 'package:hello_world_flutter/page/object_list_page.dart';
import 'package:hello_world_flutter/page/pokemon_list_page.dart';
import 'package:hello_world_flutter/page/random_cat_rest_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MenuPageWidget extends StatefulWidget {
  @override
  _MenuPageWidgetState createState() => _MenuPageWidgetState();
}

class _MenuPageWidgetState extends State<MenuPageWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Demo First App.'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.power_settings_new),
            onPressed: _logOut,
          ),
          Container(
            width: 10.0,
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Row(
              children: <Widget>[
                Spacer(),
                Expanded(
                  flex: 2,
                  child: Hero(
                    child: Image.asset("assets/kebab.jpg"),
                    tag: 'image',
                  ),
                ),
                Spacer(),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 65.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    _constructMenuButton(
                      'Calculator',
                      Icons.apps,
                      _goToCalculator,
                    ),
                    _constructMenuButton(
                      'Pokemons',
                      Icons.format_list_bulleted,
                      _goToPokemonsPage,
                    ),
                    _constructMenuButton(
                      'Random Cat',
                      Icons.pages,
                      _goToRandomCatRestPage,
                    ),
                    _constructMenuButton(
                      'Degree converter',
                      Icons.wb_sunny,
                      _goToDegreeConverter,
                    ),
                    _constructMenuButton(
                      'Hero Animation',
                      Icons.play_arrow,
                      _gotoHeroAnimationPage,
                    ),
                    _constructMenuButton(
                      'Object List',
                      Icons.list,
                      _goToObjectListPage,
                    ),
                    _constructMenuButton(
                      'Exercice Page',
                      Icons.add_to_queue,
                      _goToNetworkExercicePage,
                    ),
                    _constructMenuButton(
                      'Image Gallery',
                      Icons.image,
                      _goToGalleryPage,
                    ),
                    _constructMenuButton(
                      'Network Image',
                      Icons.network_wifi,
                      _goToNetworkImagePage,
                    ),
                    _constructMenuButton(
                      'Long List',
                      Icons.playlist_play,
                      _goToLongListPage,
                    ),
                    Container(
                      height: 21.0,
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _constructMenuButton(String name, icon, Function action) {
    return RaisedButton(
      color: Colors.blue,
      onPressed: action,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            icon,
            color: Colors.white,
          ),
          Container(
            width: 12.0,
          ),
          Text(
            name,
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );
  }

  _goToGalleryPage() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ImageGalleryPage();
    }));
  }

  _goToNetworkImagePage() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return NetWorkImagePage();
    }));
  }

  _goToNetworkExercicePage() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ExercicePage();
    }));
  }

  _goToLongListPage() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return LongListPage();
    }));
  }

  _goToObjectListPage() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ObjectListPage();
    }));
  }

  _goToCalculator() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return CalculatorPage();
    }));
  }

  _goToDegreeConverter() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return DegreeConverterPage();
    }));
  }

  _gotoHeroAnimationPage() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return HeroAnimationPage();
    }));
  }

  _logOut() async {
    print('log out');
    final pref = await SharedPreferences.getInstance();
    await pref.clear();
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LoginPage()));
  }

  _goToRandomCatRestPage() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => RandomCatRestPage()));
  }

  _goToPokemonsPage() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => PokemonListPage()));
  }
}
