import 'package:flutter/material.dart';

class LongListPage extends StatefulWidget {
  @override
  _LongListPageState createState() => _LongListPageState();
}

class _LongListPageState extends State<LongListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Long List Page'),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: 1000,
        itemBuilder: (context, index) {
          return _buildItem(index);
        },
      ),
    );
  }

  Widget _buildItem(int index) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        onTap: () {
          print('Tapped item $index');
        },
        child: Row(
          children: <Widget>[
            SizedBox(
              height: 70.0,
              width: 70.0,
              child: ClipOval(
                child: Container(
                    color: Colors.indigoAccent.withOpacity(0.2),
                    child: Image.asset('assets/car.jpg')),
              ),
            ),
            Container(
              width: 15.0,
            ),
            Expanded(
              child: Text(
                "Orphelin, le jeune Harry Potter peut enfin quitter ses tyranniques oncle et tante Dursley lorsqu'un curieux messager lui révèle qu'il est un sorcier. À 11 ans, Harry va enfin pouvoir intégrer la légendaire école de sorcellerie de Poudlard, y trouver une famille digne de ce nom et des amis, développer ses dons, et préparer son glorieux avenir.",
              ),
            )
          ],
        ),
      ),
    );
  }
}
