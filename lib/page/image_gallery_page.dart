import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImageGalleryPage extends StatefulWidget {
  @override
  _ImageGalleryPageState createState() => _ImageGalleryPageState();
}

class _ImageGalleryPageState extends State<ImageGalleryPage> {
  File _image;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Image Gallery'),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                _showSelector();
              },
            )
          ],
        ),
        body: Center(
          child:
              _image == null ? Text('No image selected.') : Image.file(_image),
        ));
  }

  _showSelector() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Pick a source'),
            content: Text('Where your picture is comming from ?'),
            actions: <Widget>[
              FlatButton(
                child: Text('Gallery'),
                onPressed: () {
                  print("GALLERYYYYY");
                },
              ),
              RaisedButton(
                child: Text('Camera'),
                onPressed: () {
                  print("CAMERAAAAAA");
                  _pickImage();
                },
              )
            ],
          );
        });
  }

  _pickImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = image;
    });
  }
}
