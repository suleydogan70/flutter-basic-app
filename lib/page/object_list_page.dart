import 'dart:math';

import 'package:flutter/material.dart';
import 'package:hello_world_flutter/model/pony_model.dart';

class ObjectListPage extends StatefulWidget {
  @override
  _ObjectListPageState createState() => _ObjectListPageState();
}

class _ObjectListPageState extends State<ObjectListPage> {
  final listAnimatedKey = GlobalKey<AnimatedListState>();
  List<Pony> ponies = [];

  @override
  void initState() {
    super.initState();

    print('init state');

    ponies.addAll([
      Pony('Bob', Colors.indigo, 19),
      Pony('Job', Colors.yellow, 250),
      Pony('Rib', Colors.red, 45),
      Pony('FORF', Colors.black, 1)
    ]);

    print(ponies.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Icon(Icons.list),
            Container(
              width: 12,
            ),
            Text('Object List'),
          ],
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            onPressed: _createPony,
            icon: Icon(Icons.add),
          )
        ],
      ),
      body: AnimatedList(
          key: listAnimatedKey,
          initialItemCount: ponies.length,
          itemBuilder: (context, index, animation) {
            Pony p = ponies[index];
            return SlideTransition(
              child: _buildPony(p),
              position: animation.drive(
                  Tween<Offset>(begin: Offset(-2.0, 0.0), end: Offset.zero)
                      .chain(CurveTween(curve: Curves.ease))),
            );
          }),
    );
  }

  Widget _buildPony(Pony pony) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 25),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              pony.name,
              style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),
            ),
          ),
          Spacer(),
          Expanded(child: Text(pony.age.toString() + ' yrs old')),
          Spacer(),
          Expanded(
            child: Container(height: 33, width: 33, color: pony.color),
          )
        ],
      ),
    );
  }

  _createPony() {
    setState(() {
      ponies.insert(
          0,
          Pony(
              'Default Pony',
              Color.fromRGBO(
                  Random.secure().nextInt(255),
                  Random.secure().nextInt(255),
                  Random.secure().nextInt(255),
                  1),
              Random.secure().nextInt(1000)));
      listAnimatedKey.currentState
          .insertItem(0, duration: Duration(milliseconds: 700));
    });
  }
}
