import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hello_world_flutter/model/user_model.dart';
import 'package:hello_world_flutter/page/menu_page.dart';
import 'package:hello_world_flutter/widget/login_button_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  // Color _loginButtonColor = Colors.deepPurple;

  bool _isLoading = false;
  bool _formError = false;

  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.account_circle),
            Container(width: 12.0),
            Text('Login Page'),
          ],
        ),
      ),
      body: _chargeBodyContent(),
    );
  }

  Widget _chargeBodyContent() {
    if (_isLoading) {
      return _progressIndicator();
    } else {
      return _contentLoginForm();
    }
  }

  Widget _progressIndicator() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _contentLoginForm() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(height: 50.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.account_circle,
                size: 64.0,
                color: Colors.blue,
              ),
            ],
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 70.0, vertical: 8.0),
            child: Column(
              children: <Widget>[
                TextField(
                  controller: _usernameController,
                  decoration: InputDecoration(
                    labelText: 'Username',
                    isDense: true,
                    focusedBorder: _textFieldBorderStyles(isFocused: true),
                    enabledBorder: _textFieldBorderStyles(),
                  ),
                ),
                Container(height: 16.0),
                TextField(
                  controller: _passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    labelText: 'Password',
                    isDense: true,
                    focusedBorder: _textFieldBorderStyles(isFocused: true),
                    enabledBorder: _textFieldBorderStyles(),
                  ),
                ),
                Container(height: 20.0),
                /*GestureDetector(
                    onTapDown: _onTapDownButton,
                    onTapCancel: __onTapCancelButton,
                    onTapUp: __onTapUpButton,
                    child: StaticLoginButtonWidget(_loginButtonColor),
                  ),*/
                LoginButtonWidget('Connect', _buttonConnectPressed),
                Container(height: 16.0),
                LoginButtonWidget('Register', _buttonRegisterPressed),
                Container(height: 16.0),
                _displayError()
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _displayError() {
    if (_formError) {
      return Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(242, 222, 222, 1.0),
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
          child: Row(
            children: <Widget>[
              Icon(
                Icons.warning,
                color: Color.fromRGBO(169, 68, 66, 1.0),
              ),
              Text(
                'Username or Password incorrect',
                style: TextStyle(
                  color: Color.fromRGBO(169, 68, 66, 1.0),
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  _textFieldBorderStyles({bool isFocused = false}) {
    return OutlineInputBorder(
      borderSide: BorderSide(
        color: isFocused ? Colors.blue : Colors.grey,
        width: 1.0,
      ),
    );
  }

  _buttonConnectPressed() async {
    print('Login Pressed');
    setState(() {
      _isLoading = true;
    });

    String username = _usernameController.text;
    String password = _passwordController.text;

    await Future.delayed(Duration(seconds: 1));

    if (username == 'admin' && password == 'admin') {
      User user = User(username, password);

      String encodedUser = jsonEncode(user.toJSON());
      print(encodedUser);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('user', encodedUser);

      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => MenuPageWidget()),
      );
    } else {
      setState(() {
        _isLoading = false;
        _formError = true;
      });
    }
  }

  _buttonRegisterPressed() {
    print('Register Pressed');
    setState(() {
      _isLoading = true;
    });

    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        _isLoading = false;
      });
    });
  }

  /*_onTapDownButton(detail) {
    setState(() {
      _loginButtonColor = Colors.red;
    });
  }

  __onTapCancelButton() {
    setState(() {
      _loginButtonColor = Colors.deepPurple;
    });
  }

  __onTapUpButton(detail) {
    setState(() {
      _loginButtonColor = Colors.deepPurple;
    });
  }*/
}
