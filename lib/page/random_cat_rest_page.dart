import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hello_world_flutter/model/cat_model.dart';
import 'package:hello_world_flutter/rest/cat_api.dart';

class RandomCatRestPage extends StatefulWidget {
  @override
  _RandomCatRestPageState createState() => _RandomCatRestPageState();
}

class _RandomCatRestPageState extends State<RandomCatRestPage> {
  Cat _currentCat;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Random cat'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            onPressed: _getRandomCat,
            icon: Icon(Icons.refresh),
          ),
        ],
      ),
      body: _currentCat != null
          ? Column(
              children: <Widget>[
                Expanded(
                  child: CachedNetworkImage(
                    imageUrl: _currentCat.imageURL ?? '',
                    placeholder: (context, url) => Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: 48.0,
                          height: 48.0,
                          child: CircularProgressIndicator(),
                        ),
                      ],
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ],
            )
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Center(child: Text('No current cat selected')),
              ],
            ),
    );
  }

  _getRandomCat() async {
    Cat cat = await CatApi.getRandomCat();
    setState(() {
      _currentCat = cat;
    });
  }
}
