import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hello_world_flutter/model/pokemon_model.dart';

class SinglePokemonPage extends StatefulWidget {
  final Pokemon pokemon;

  SinglePokemonPage(this.pokemon);

  @override
  _SinglePokemonPageState createState() => _SinglePokemonPageState();
}

class _SinglePokemonPageState extends State<SinglePokemonPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.account_circle),
            Container(width: 4.0),
            Text(widget.pokemon.name)
          ],
        ),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: CachedNetworkImage(
              imageUrl: widget.pokemon.imageURL,
              placeholder: (context, url) => SizedBox(
                width: 48.0,
                height: 48.0,
                child: CircularProgressIndicator(),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: GridView.count(
                crossAxisCount: 3,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text(
                        'Types',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18.0),
                      ),
                      Column(
                        children: widget.pokemon.types.length == 0
                            ? [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('missing'),
                                )
                              ]
                            : widget.pokemon.types
                                .map((type) => Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(type),
                                    ))
                                .toList(),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        'HP',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18.0),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(widget.pokemon.hp == ''
                            ? 'missing'
                            : widget.pokemon.hp),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        'Rarity',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18.0),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(widget.pokemon.rarity == ''
                            ? 'missing'
                            : widget.pokemon.rarity),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        'Weaknesses',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18.0),
                      ),
                      Column(
                        children: widget.pokemon.weaknesses.length == 0
                            ? [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('missing'),
                                )
                              ]
                            : widget.pokemon.weaknesses
                                .map((weakness) => Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(weakness['type'] +
                                          ' : ' +
                                          weakness['value']),
                                    ))
                                .toList(),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        'Resistances',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18.0),
                      ),
                      Column(
                        children: widget.pokemon.resistances.length == 0
                            ? [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('missing'),
                                )
                              ]
                            : widget.pokemon.resistances
                                .map((resistance) => Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(resistance['type'] +
                                          ' : ' +
                                          resistance['value']),
                                    ))
                                .toList(),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        'Attacks',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18.0),
                      ),
                      Column(
                        children: widget.pokemon.attacks.length == 0
                            ? [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('missing'),
                                )
                              ]
                            : widget.pokemon.attacks
                                .map((attack) => Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(attack['name'] +
                                          ' : ' +
                                          attack['damage']),
                                    ))
                                .toList(),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
