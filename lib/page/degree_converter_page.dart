import 'package:flutter/material.dart';

class DegreeConverterPage extends StatefulWidget {
  @override
  _DegreeConverterPageState createState() => _DegreeConverterPageState();
}

class _DegreeConverterPageState extends State<DegreeConverterPage> {
  TextEditingController _c = TextEditingController();
  TextEditingController _f = TextEditingController();
  bool _errorFired = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.wb_sunny),
            Container(width: 12.0),
            Text('Degree Converter')
          ],
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _generateRowField('°C', _c),
          Container(width: 12.0),
          _displayError(),
          Container(width: 12.0),
          _generateRowField('°F', _f)
        ],
      ),
    );
  }

  Widget _generateRowField(String title, TextEditingController controller) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 100.0),
            child: TextField(
              onChanged: (text) {
                _onChangeField(text, title);
              },
              controller: controller,
              decoration: InputDecoration(
                labelText: title,
                isDense: true,
                focusedBorder: _textFieldBorderStyles(isFocused: true),
                enabledBorder: _textFieldBorderStyles(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  _onChangeField(text, title) {
    double value;
    if (text != '') {
      try {
        value = double.parse(text);
        if (title == '°C') {
          _f.text = ((value * 9 / 5) + 32).toString();
        } else {
          _c.text = ((value - 32) * 5 / 9).toString();
        }
        setState(() {
          _errorFired = false;
        });
      } catch (err) {
        setState(() {
          _errorFired = true;
        });
      }
    } else {
      _c.text = '';
      _f.text = '';
    }
  }

  Widget _displayError() {
    if (_errorFired) {
      return Container(
        height: 50.0,
        color: Color.fromRGBO(242, 222, 222, 1.0),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.warning,
                color: Color.fromRGBO(169, 68, 66, 1.0),
              ),
              Container(
                width: 5.0,
              ),
              Text(
                'Enter something correct please',
                style: TextStyle(
                  color: Color.fromRGBO(169, 68, 66, 1.0),
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Container(height: 50.0);
    }
  }

  _textFieldBorderStyles({bool isFocused = false}) {
    return OutlineInputBorder(
      borderSide: BorderSide(
        color: isFocused ? Colors.blue : Colors.grey,
        width: 1.0,
      ),
    );
  }
}
