import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hello_world_flutter/model/pokemon_model.dart';
import 'package:hello_world_flutter/page/single_pokemon_page.dart';
import 'package:hello_world_flutter/rest/pokemon_api.dart';

class PokemonListPage extends StatefulWidget {
  @override
  _PokemonListPageState createState() => _PokemonListPageState();
}

class _PokemonListPageState extends State<PokemonListPage> {
  List<Pokemon> _pokemons = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.format_list_bulleted),
              Container(width: 8.0),
              Text('Pokemons'),
            ],
          ),
        ),
        body: _displayPokemons());
  }

  _getPokemons() async {
    var res = await PokemonApi.getPokemons();
    setState(() {
      print('getting');
      _pokemons = res;
      print(_pokemons);
    });
  }

  _displayPokemons() {
    if (_pokemons.length > 0) {
      return GridView.count(
        crossAxisCount: 2,
        padding: const EdgeInsets.all(4.0),
        mainAxisSpacing: 4.0,
        crossAxisSpacing: 4.0,
        children: _pokemons
            .map(
              (pokemon) => GestureDetector(
                onTap: () {
                  _goToPokemonPage(pokemon);
                },
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: CachedNetworkImage(
                        imageUrl: pokemon.imageURL,
                        placeholder: (context, url) => SizedBox(
                          width: 48.0,
                          height: 48.0,
                          child: CircularProgressIndicator(),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(
                          Radius.circular(8.0),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            pokemon.name,
                            style: TextStyle(
                                fontSize: 17.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          Icon(
                            Icons.last_page,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
            .toList(),
      );
    } else {
      _getPokemons();
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(child: Text('Getting Pokemons...')),
        ],
      );
    }
  }

  _goToPokemonPage(Pokemon pokemon) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => SinglePokemonPage(pokemon)));
  }
}
