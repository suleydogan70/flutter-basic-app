import 'dart:convert';

import 'package:hello_world_flutter/model/pokemon_model.dart';
import 'package:http/http.dart' as http;

class PokemonApi {
  static final String pokemonsURL = 'https://api.pokemontcg.io/v1/cards';
  static List<Pokemon> pokemonList = [];

  static Future<List<Pokemon>> getPokemons() async {
    var pokemons = await http.get(pokemonsURL);
    List pokemonsMap;

    if (pokemons.statusCode != 200) {
      return null;
    }
    pokemonsMap = jsonDecode(pokemons.body)['cards'];

    for (var i = 0; i < pokemonsMap.length; i++) {
      pokemonList.add(Pokemon.fromJSON(pokemonsMap[i]));
    }

    return pokemonList;
  }
}
