import 'package:flutter/material.dart';

class LoginButtonWidget extends StatefulWidget {
  final String title;
  final Function buttonPressed;

  LoginButtonWidget(this.title, this.buttonPressed);

  @override
  _LoginButtonWidgetState createState() => _LoginButtonWidgetState();
}

class _LoginButtonWidgetState extends State<LoginButtonWidget> {
  Color _color;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _color = Colors.deepPurple;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: _onTapDownButton,
      onTapCancel: __onTapCancelButton,
      onTapUp: __onTapUpButton,
      child: Container(
        decoration: BoxDecoration(
          color: _color,
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: Center(
            child: Text(
              widget.title,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }

  _onTapDownButton(detail) {
    setState(() {
      _color = Colors.green.withOpacity(0.8);
    });
  }

  __onTapCancelButton() {
    setState(() {
      _color = Colors.deepPurple;
    });
  }

  __onTapUpButton(detail) {
    setState(() {
      widget.buttonPressed();
      _color = Colors.deepPurple;
    });
  }
}
